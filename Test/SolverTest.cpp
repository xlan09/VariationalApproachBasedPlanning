#include "catch.hpp"
#include "VelocityProfile.h"
#include "Solver.h"
#include "State.h"
#include "Vehicle.h"
#include "Util.h"
#include <iostream>
#include <cmath>

const double PI=3.1415926;

template<typename T>
void printVec(const std::vector<T> &res)
{
    if(res.empty())
    {
        std::cout<< "Vec is empty!" << std::endl;
    }
    else
    {
        for(unsigned int i = 0; i < res.size(); ++i)
        {
            std::cout << res[i] << ", ";
        }

        std::cout<< std::endl;
    }
}

TEST_CASE("Profile Test")
{
    VelocityProfile profile(0, 6, 2);
    REQUIRE(profile.velocity(2) == 4);
    REQUIRE(profile.velocity(7) == 6);
}

TEST_CASE("Arange Test")
{
    std::vector<double> res = Util::arange(0, 3.3, 0.1);
    printVec(res);

    res = Util::arange(0, 3.33, 0.1);
    printVec(res);

    res = Util::arange(0, 0, 0.1);
    printVec(res);

    res = Util::arange(-2, 6.5, 0.2);
    printVec(res);

    res = Util::arange(1.3, 5.6, 0.3);
    printVec(res);

    res = Util::arange(-7.8, -1.3, 0.4);
    printVec(res);

    res = Util::arange(-3.4, -9.8, -0.25);
    printVec(res);

    res = Util::arange(9.8, -7.8, -0.33);
    printVec(res);
}

TEST_CASE("Util find insertion test")
{
    std::cout<< "Find insertion position test" << std::endl;
    std::map<double, double> mapping;
    mapping[0] = 10;
    mapping[1] = 11;
    mapping[2] = 12;

    REQUIRE(Util::findInsertionPosition(mapping, -2.0) == 0);
    REQUIRE(Util::findInsertionPosition(mapping, 0.0) == 0);
    REQUIRE(Util::findInsertionPosition(mapping, 0.2) == 0);
    REQUIRE(Util::findInsertionPosition(mapping, 0.8) == 1);
    REQUIRE(Util::findInsertionPosition(mapping, 1.0) == 1);
    REQUIRE(Util::findInsertionPosition(mapping, 1.2) == 1);
    REQUIRE(Util::findInsertionPosition(mapping, 1.6) == 2);
    REQUIRE(Util::findInsertionPosition(mapping, -2.0) == 0);
    REQUIRE(Util::findInsertionPosition(mapping, 2.0) == 2);
    REQUIRE(Util::findInsertionPosition(mapping, 4.0) == 2);
}

TEST_CASE("Solver Test")
{
    std::cout << "---------------------------------------------------------" << std::endl;
    std::cout << "Solver algorithm test" << std::endl;
    double timeStep = 0.1, paraVariance = 0.01, tolerance = 0.015;
    double maxLinearVel = 0.6, w0 = 0, wf = 0;
    double v0 = maxLinearVel;
    double vf = maxLinearVel;
    double startHeading = 0;
    double dist = 2;

    Pose startPose(0, 0, startHeading);
    Vehicle vehicle(startPose, v0, w0);
    State startState(startPose, v0, w0);

    double initParaArray[] = {0, 0.01, 0.02, 0.03, dist / maxLinearVel};
    std::vector<double> initPara(initParaArray, initParaArray + 5);

    std::vector<double> goalStateHeading = Util::arange(0, PI, PI/20);
    for(unsigned int i = 0; i < goalStateHeading.size(); ++i)
    {
        State goalState(Pose(dist, 0, goalStateHeading[i]), vf, wf);
        std::vector<double> optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
        initPara = optimalPara;
    }
}

/**
 * @brief TEST_CASE
 * E.g: If optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, PI/20, 0.6, 0] where state=[x, y, theta, linearVel, angularVel] is [x1, x2, x3, x4, finalTime], then the optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, -PI/20, 0.6, 0] should be [-x1, -x2, -x3, -x4, finalTime]. The first four parameters are the middle control points for the 5-th degree spline used to parametrize the angular velocity.
 */
TEST_CASE("Solver parameter symmetric about x axis test")
{
    std::cout << "######################################################" << std::endl;
    std::cout << "start pos and goal pos is on x axis" << std::endl;
    double timeStep = 0.1, paraVariance = 0.01, tolerance = 0.015;
    double maxLinearVel = 0.6, w0 = 0, wf = 0;
    double v0 = maxLinearVel;
    double vf = maxLinearVel;
    double startHeading = 0;
    double dist = 2;

    Pose startPose(0, 0, startHeading);
    Vehicle vehicle(startPose, v0, w0);
    State startState(startPose, v0, w0);

    double initParaArray[] = {0, 0.01, 0.02, 0.03, dist / maxLinearVel};
    std::vector<double> initPara(initParaArray, initParaArray + 5);

    double goalHeading = PI/20.0;
    State goalState(Pose(dist, 0, goalHeading), vf, wf);
    std::vector<double> optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    goalHeading = -PI/20.0;
    goalState = State(Pose(dist, 0, goalHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    std::cout << "######################################################" << std::endl;
    std::cout << "start pos and goal pos is not along x axis, they are on line y = (tan(PI / 6) * x)" << std::endl;
    startHeading = PI / 3;
    goalHeading = PI / 3 + PI / 20;
    startState = State(Pose(0, 0, startHeading), vf, wf);
    goalState = State(Pose(std::sqrt(3), 1, goalHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    startHeading = PI / 3;
    goalHeading = PI / 3 - PI / 20;
    startState = State(Pose(0, 0, startHeading), vf, wf);
    goalState = State(Pose(std::sqrt(3), 1, goalHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);
}

/**
 * @brief TEST_CASE
 * E.g: If optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, PI/20, 0.6, 0] where state=[x, y, theta, linearVel, angularVel] is [x1, x2, x3, x4, finalTime], then the optimal para for startState=[0, 0, PI/20, 0.6, 0] and goalState=[2, 0, 0, 0.6, 0] should be [-x4, -x3, -x2, -x1, finalTime]. The first four parameters are the middle control points for the 5-th degree spline used to parametrize the angular velocity.
 */
TEST_CASE("Solver parameter symmetric when switching start and goal state")
{
    std::cout << "######################################################" << std::endl;
    std::cout << "start pos and goal pos is on x axis" << std::endl;
    double timeStep = 0.1, paraVariance = 0.01, tolerance = 0.015;
    double maxLinearVel = 0.6, w0 = 0, wf = 0;
    double v0 = maxLinearVel;
    double vf = maxLinearVel;
    double startHeading = 0;
    double dist = 2;

    Pose startPose(0, 0, startHeading);
    Vehicle vehicle(startPose, v0, w0);
    State startState(startPose, v0, w0);

    double initParaArray[] = {0, 0.01, 0.02, 0.03, dist / maxLinearVel};
    std::vector<double> initPara(initParaArray, initParaArray + 5);

    double goalHeading = PI/20.0;
    State goalState(Pose(dist, 0, goalHeading), vf, wf);
    std::vector<double> optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    goalHeading = 0;
    startHeading = PI/20;
    goalState = State(Pose(dist, 0, goalHeading), vf, wf);
    startState = State(Pose(0, 0, startHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    std::cout << "######################################################" << std::endl;
    std::cout << "start pos and goal pos is not along x axis, they are on line y = (tan(PI / 6) * x)" << std::endl;
    startHeading = PI / 3;
    goalHeading = PI / 20 + PI / 3;
    startState = State(Pose(0, 0, startHeading), vf, wf);
    goalState = State(Pose(std::sqrt(3), 1, goalHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);

    startHeading = PI / 20 + PI / 3;
    goalHeading = PI / 3;
    startState = State(Pose(0, 0, startHeading), vf, wf);
    goalState = State(Pose(std::sqrt(3), 1, goalHeading), vf, wf);
    optimalPara = Solver::solve(vehicle, startState, goalState, timeStep, initPara, paraVariance, tolerance);
    std::cout<< "----------------------------------------------------------" << std::endl;
    std::cout << "Optimal parameters for startState= " << startState.toString() << ", goalState=" << goalState.toString() << std::endl;
    printVec(optimalPara);
}

TEST_CASE("Solver Service Test")
{
    SolverService service(0, 0, 2, 0, 0.6);
    service.run(0, PI / 20.0);
    service.run(PI / 3, PI/20.0);
    service.run(2 * PI / 3, PI / 3);
}
