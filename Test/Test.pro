TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    SolverTest.cpp

LIBS += $$PWD/../build*/Solver/libSolver.a -lpthread

INCLUDEPATH += $$PWD/../Solver $$PWD/../lib
