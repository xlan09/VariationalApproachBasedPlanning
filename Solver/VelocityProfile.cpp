#include "VelocityProfile.h"
#include <stdexcept>
#include "Util.h"

VelocityProfile::VelocityProfile(double v0, double vf, double maxAcceleration) : m_v0(v0), m_vf(vf), m_maxAcceleration(maxAcceleration)
{
    if(m_v0 < 0 || m_vf < 0 || m_maxAcceleration <= 0)
    {
        throw std::invalid_argument("velocities must be non-negative and max accelerations must be positive");
    }
}

double VelocityProfile::velocity(double time) const
{
    double res = 0;
    double acceleration = m_maxAcceleration;
    if (m_vf < m_v0)
    {
        acceleration = -1 * m_maxAcceleration;
    }
    else if (Util::isDoubleEqual(m_vf, m_v0))
    {
        acceleration = 0;
    }

    if(Util::isDoubleEqual(acceleration, 0))
    {
        res = m_vf;
    }
    else
    {
        double timeToReachFinalVelocity = (m_vf - m_v0) / acceleration;
        if(time <= timeToReachFinalVelocity)
        {
            res = m_v0 + acceleration * time;
        }
        else
        {
            res = m_vf;
        }
    }

    return res;
}

