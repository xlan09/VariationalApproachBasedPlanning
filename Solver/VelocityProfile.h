#ifndef VELOCITYPROFILE_H
#define VELOCITYPROFILE_H


class VelocityProfile
{
public:
    VelocityProfile(double v0, double vf, double maxAcceleration=0.3);

    /**
     * @brief velocity
     * Get the velocity at specific time
     */
    double velocity(double time) const;

private:
    double m_v0;
    double m_vf;
    double m_maxAcceleration;
};

#endif // VELOCITYPROFILE_H
