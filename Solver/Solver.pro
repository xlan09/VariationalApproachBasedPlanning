#-------------------------------------------------
#
# Project created by QtCreator 2016-11-23T16:53:12
#
#-------------------------------------------------

QT       -= core gui

TARGET = Solver
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += Solver.cpp \
    Util.cpp \
    Spline.cpp \
    VelocityProfile.cpp \
    AngularProfile.cpp \
    State.cpp \
    Pose.cpp \
    Vehicle.cpp \
    DataStore.cpp

HEADERS += Solver.h \
    Util.h \
    Spline.h \
    VelocityProfile.h \
    AngularProfile.h \
    State.h \
    Pose.h \
    Vehicle.h \
    DataStore.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lpthread
INCLUDEPATH += $$PWD/../lib
