#include "Pose.h"
#include "Util.h"
#include <stdexcept>

Pose::Pose() : m_x(0), m_y(0), m_theta(0)
{

}

Pose::Pose(double x, double y, double theta) : m_x(x), m_y(y), m_theta(theta)
{

}

Pose &Pose::operator +=(const Pose &pose)
{
    m_x += pose.m_x;
    m_y += pose.m_y;
    m_theta += pose.m_theta;

    return *this;
}

Pose &Pose::operator -=(const Pose &pose)
{
    m_x -= pose.m_x;
    m_y -= pose.m_y;
    m_theta -= pose.m_theta;

    return *this;
}

Pose &Pose::operator *=(double num)
{
    m_x *= num;
    m_y *= num;
    m_theta *= num;

    return *this;
}
double Pose::x() const
{
    return m_x;
}
double Pose::y() const
{
    return m_y;
}
double Pose::theta() const
{
    return m_theta;
}

std::string Pose::toString() const
{
    return "x: " + std::to_string(m_x) + ", y: " + std::to_string(m_y) + ", theta: " + std::to_string(m_theta);
}

Pose operator +(const Pose &lhs, const Pose &rhs)
{
    Pose sum = lhs;
    sum += rhs;

    return sum;
}

Pose operator -(const Pose &lhs, const Pose &rhs)
{
    Pose remainder = lhs;
    remainder -= rhs;

    return remainder;
}


Pose operator *(const Pose &val, double num)
{
    Pose product = val;
    product *= num;

    return product;
}


Pose operator *(double num, const Pose &val)
{
    return val * num;
}

Pose operator /(const Pose &val, double num)
{
    if (Util::isDoubleEqual(num, 0))
    {
        throw std::invalid_argument("Pose can not be divided by zero");
    }

    return val * (1.0 / num);
}


Pose operator /(double num, const Pose &val)
{
    return val / num;
}
