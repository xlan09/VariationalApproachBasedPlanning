#ifndef POSE_H
#define POSE_H
#include <string>

class Pose
{
public:
    Pose();
    Pose(double x, double y, double theta);

    Pose &operator +=(const Pose &pose);
    Pose &operator -=(const Pose &pose);
    Pose &operator *=(double num);

    double x() const;

    double y() const;

    double theta() const;

    std::string toString() const;

private:
    double m_x;
    double m_y;
    double m_theta;
};

Pose operator +(const Pose &lhs, const Pose &rhs);
Pose operator -(const Pose &lhs, const Pose &rhs);
Pose operator *(const Pose &val, double num);
Pose operator *(double num, const Pose &val);
Pose operator /(const Pose &val, double num);
Pose operator /(double num, const Pose &val);

#endif // POSE_H
