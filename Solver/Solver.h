#ifndef SOLVER_H
#define SOLVER_H

#include <vector>
#include <memory>
#include <map>
#include <utility>

class Vehicle;
class State;
class VelocityProfile;
class AngularProfile;
class DataStore;
namespace spdlog
{
class logger;
}

class Solver
{
public:
    /**
     * @brief solve
     * The parameters for the solver is: [middle control_points_for_spline_of_angular_velocity, finalTime]
     * We also use the angular velocity at the start state and the goal state as control points, so the degree of the spline
     * we use is num of (middle control_points_for_spline_of_angular_velocity) + 2 - 1
     */
    static std::vector<double> solve(const Vehicle &vehicle, const State &startState, const State &goalState, double timeStep, const std::vector<double> &initPara, double paraVariance, double tolerance, int maxIter=50);

    static State forwardSimulation(const Vehicle &vehicle, const State &startState, const VelocityProfile &velocityProfile, const AngularProfile &angularProfile, double finalTime, double timeStep, std::vector<std::pair<double, double>> &trajectory);

private:
    static State calculatePartialDerirativeForJthPara(const Vehicle &vehicle, const State &startState, const State &goalState, const std::vector<double> &currPara, double paraVariance, double timeStep, unsigned int j);
    const static std::shared_ptr<spdlog::logger> ms_logger;
};


class SolverService
{
public:
    SolverService(double startPosX, double startPosY, double goalPosX, double goalPosY, double maxLinearVel);

    /**
     * @brief run
     * @param vehicle
     * @param startState
     * @param goalState
     */
    void run(double startHeading, double goalHeading);

    /**
     * @brief havePointsToDraw
     * Check whether we have any trajectory to draw
     * @return
     */
    bool havePointsToDraw() const;

    /**
     * @brief popPoint
     * Pop point out of queue
     * @return
     */
    std::pair<double, double> popPoint() const;

private:
    double m_startPosX;
    double m_startPosY;
    double m_goalPosX;
    double m_goalPosY;
    double m_maxLinearVel; // max linear velocity of the vehicle
    double m_w0;
    double m_wf;
    double m_timeStep;
    double m_headingResolution;
    std::shared_ptr<DataStore> m_dataStore;
    std::shared_ptr<spdlog::logger> m_logger;
    std::map<double, std::map<double, std::vector<double> >> m_paraLookupTable; // first key is startHeading, second key is goalHeading

    void initLookupTable();

    /**
     * @brief runSolver
     * Run solver with given initial parameters
     * @return optimal parameters
     */
    std::vector<double> runSolver(double startHeading, double goalHeading, const std::vector<double> initPara) const;

    /**
     * @brief findParameter
     * find initial parameter for one startHeading and goalHeading combination
     * @param startHeading
     * @param goalHeading
     * @return
     */
    std::pair<double, double> findClosestParametersInLookupTable(double startHeading, double goalHeading) const;

    /**
     * @brief incrementallySearchParameters
     * @param initialHeading
     * @param finalHeading
     * @param headingResolution
     * @param initPara
     * @return
     */
    std::vector<double> incrementallySearchParameters(double fixedHeading, double variedInitialHeading, double variedFinalHeading, double headingResolution, const std::vector<double> &paraToStart, bool incrementStartHeading) const;

    /**
     * @brief getTrajectory
     * @param startHeading
     * @param goalHeading
     * @param trajectory
     */
    void getTrajectory(double startHeading, double goalHeading, const std::vector<double> &para, std::vector<std::pair<double, double>> &trajectory) const;

    /**
     * @brief printTrajectoryToFile
     * Serialize trajectory
     * @param trajectory
     */
    void printTrajectoryToFile(const std::vector<std::pair<double, double>> &trajectory) const;


    /**
     * @brief addTrajectoryToQueue
     * @param trajectory
     */
    void addTrajectoryToQueue(const std::vector<std::pair<double, double> > &trajectory) const;

    /**
     * @brief addParaToLookUpTable
     * @param startHeading
     * @param goalHeading
     * @param para
     */
    void addParaToLookUpTable(double startHeading, double goalHeading, const std::vector<double> &para);
};

#endif // SOLVER_H
