#ifndef VEHICLE_H
#define VEHICLE_H

#include "Pose.h"

class Vehicle
{
public:
    Vehicle(const Pose &initPose, double initialLinearVelocity, double initialAngularVelocity);

    Pose pose() const;

    double linearVelocity() const;
    void setLinearVelocity(double linearVelocity);

    double angularVelocity() const;
    void setAngularVelocity(double angularVelocity);

    /**
     * @brief dynamics
     * Vehicle dynamics
     */
    Pose dynamics(const Pose &currPose, double linearVelocity, double angularVelocity, double dt) const;

private:
    Pose m_pose;
    double m_linearVelocity;
    double m_angularVelocity;
};

#endif // VEHICLE_H
