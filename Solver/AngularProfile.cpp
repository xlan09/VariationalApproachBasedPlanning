#include "AngularProfile.h"

AngularProfile::AngularProfile(const std::vector<Point> &controlPoints, double t) : m_spline(Spline(controlPoints, controlPoints.size() - 1, t))
{

}

Point AngularProfile::angularVelocity(double time) const
{
    return m_spline.value(time);
}

