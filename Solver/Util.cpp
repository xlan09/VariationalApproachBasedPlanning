#include "Util.h"
#include <cmath>
#include <stdexcept>
#include "State.h"
#include "VelocityProfile.h"
#include "AngularProfile.h"
#include <string>
#include "spdlog/spdlog.h"

bool Util::isDoubleEqual(double lhs, double rhs)
{
    double tolerance = std::pow(10, -14);
    return std::fabs(lhs - rhs) < tolerance;
}

std::vector<double> Util::linspace(double start, double end, unsigned int step)
{
    if (end <= start) throw std::invalid_argument("end must be greater than start");
    if (step <= 1) throw std::invalid_argument("step must be greater than 1");
    double interval = (end - start) / (step - 1);

    double element = start;
    std::vector<double> res;
    while(element < end)
    {
        res.push_back(element);
        element += interval;
    }

    res.push_back(end);

    return res;
}

std::vector<double> Util::arange(double start, double end, double interval)
{
    if (isDoubleEqual(interval, 0)) throw std::invalid_argument("interval can not be 0");
    double numOfIntervals = (end - start) / interval;
    if(numOfIntervals < 0) throw std::invalid_argument("(end - start) must have the same sign with interval");

    std::vector<double> res;
    if (isDoubleEqual(numOfIntervals, 0) == false)
    {
        double lastNum = start;
        while(1)
        {
            res.push_back(lastNum);
            lastNum += interval;

            if ((interval > 0 && lastNum >= end) || (interval < 0 && lastNum <= end))
            {
                break;
            }
        }
    }

    return res;
}

Eigen::VectorXd Util::convertToEigenVector(const std::vector<double> &vec)
{
    Eigen::VectorXd res(vec.size());
    for(unsigned int i = 0; i < vec.size(); ++i)
    {
        res(i) = vec[i];
    }

    return res;
}

Eigen::VectorXd Util::convertToEigenVector(const State &state)
{
    Eigen::VectorXd res(5);
    Pose statePose = state.pose();
    res(0) = statePose.x();
    res(1) = statePose.y();
    res(2) = statePose.theta();
    res(3) = state.linearVelocity();
    res(4) = state.angularVelocity();

    return res;
}

std::vector<double> Util::convertToStdVector(const Eigen::VectorXd &eigenVec)
{
    std::vector<double> res;
    for(int i = 0; i < eigenVec.rows(); ++i)
    {
        res.push_back(eigenVec(i));
    }

    return res;
}

std::vector<Point> Util::generateControlPoints(const std::vector<double> &values)
{
    std::vector<Point> res;
    for(std::size_t i = 0; i < values.size(); ++i)
    {
        res.push_back(Point(values[i]));
    }

    return res;
}

std::pair<VelocityProfile, AngularProfile> Util::convertParametersToProfiles(const State &startState, const State &goalState, const std::vector<double> &para)
{
    VelocityProfile velocityProfile(startState.linearVelocity(), goalState.linearVelocity());

    std::vector<double> points;
    points.push_back(startState.angularVelocity());
    for(int i = 0; i < (int)para.size() - 1; ++i)
    {
        points.push_back(para[i]);
    }
    points.push_back(goalState.angularVelocity());

    AngularProfile angularProfile(Util::generateControlPoints(points), para.back());

    return std::make_pair(velocityProfile, angularProfile);
}

std::shared_ptr<spdlog::logger> Util::getLogger(const std::string &loggerName, const std::string &loggerFilename)
{
    std::shared_ptr<spdlog::logger> logger = spdlog::get(loggerName);
    if(logger == nullptr)
    {
        size_t q_size = 4096; //queue size must be power of 2
        spdlog::set_async_mode(q_size);
        logger = spdlog::rotating_logger_mt(loggerName, loggerFilename, 1024 * 1024 * 10, 5);
    }

    return logger;
}

/**
 * Here we use scan directly, binary search would also be OK, but first we need to get all the keys. copying also takes O(n) time
 */
template<typename T1, typename T2>
T1 Util::findInsertionPosition(const std::map<T1, T2> &mapping, T1 val)
{
    if(mapping.size() == 0) throw std::invalid_argument("Mapping can not be empty when searching for insertion position");

    typename std::map<T1, T2>::const_iterator previousIter = mapping.begin();
    bool isPreviousIterValid = false;
    T1 refStartHeading = mapping.rbegin()->first;
    for(typename std::map<T1, T2>::const_iterator iter = mapping.begin(); iter != mapping.end(); ++iter)
    {
        if(val <= iter->first)
        {
            refStartHeading = iter->first;
            if (isPreviousIterValid && std::abs(previousIter->first - val) < std::abs(iter->first - val))
            {
                refStartHeading = previousIter->first;
            }

            break;
        }
        else
        {
            isPreviousIterValid = true;
            previousIter = iter;
        }
    }

    return refStartHeading;
}

template<typename T>
std::string Util::toString(const std::vector<T> &val)
{
    std::string res;
    for(unsigned int i = 0; i < val.size(); ++i)
    {
        if (i != 0) res += ", ";
        res += std::to_string(val[i]);
    }

    return res;
}

template<typename MatT>
std::string Util::toString(const MatT &mat)
{
    std::string res;
    for(int i = 0; i < mat.rows(); ++i)
    {
        for(int j = 0; j < mat.cols(); ++j)
        {
            if (j != 0) res += ", ";
            res += std::to_string(mat(i, j));
        }

        res += "\n";
    }

    return res;
}

template <typename MatT>
Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> Util::pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance)
{
    typedef typename MatT::Scalar Scalar;
    auto svd = mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
    const auto &singularValues = svd.singularValues();
    Eigen::Matrix<Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime> singularValuesInv(mat.cols(), mat.rows());
    singularValuesInv.setZero();
    for (unsigned int i = 0; i < singularValues.size(); ++i)
    {
        if (singularValues(i) > tolerance)
        {
            singularValuesInv(i, i) = Scalar{1} / singularValues(i);
        }
        else
        {
            singularValuesInv(i, i) = Scalar{0};
        }
    }
    return svd.matrixV() * singularValuesInv * svd.matrixU().adjoint();
}

template Eigen::MatrixXd Util::pseudoinverse<Eigen::MatrixXd>(const Eigen::MatrixXd &, double);
template std::string Util::toString(const std::vector<double> &);
template std::string Util::toString(const Eigen::MatrixXd &);
template double Util::findInsertionPosition(const std::map<double, std::map<double, std::vector<double>>> &, double);
        template double Util::findInsertionPosition(const std::map<double, double> &, double);
        template double Util::findInsertionPosition(const std::map<double, std::vector<double>> &, double);
