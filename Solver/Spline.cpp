#include "Spline.h"
#include <stdexcept>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <chrono>
#include "spdlog/spdlog.h"
#include "Util.h"

Point::Point() : m_x(0), m_y(0)
{

}

Point::Point(double x) : m_x(x), m_y(0)
{

}

Point::Point(double x, double y) : m_x(x), m_y(y)
{

}

double &Point::operator[](unsigned int i)
{
    if(i == 0) return m_x;
    else if(i == 1) return m_y;
    else throw std::invalid_argument("Index out of range when accessing point!");
}

const double &Point::operator [](unsigned int i) const
{
    if(i == 0) return m_x;
    else if(i == 1) return m_y;
    else throw std::invalid_argument("Index out of range when accessing point!");
}

Point &Point::operator +=(const Point &rhs)
{
    m_x += rhs.m_x;
    m_y += rhs.m_y;
    return *this;
}

Point &Point::operator +=(double rhs)
{
    m_x += rhs;
    m_y += rhs;

    return *this;
}

Point &Point::operator -=(const Point &rhs)
{
    m_x -= rhs.m_x;
    m_y -= rhs.m_y;

    return *this;
}

Point &Point::operator -=(double rhs)
{
    m_x -= rhs;
    m_y -= rhs;

    return *this;
}

Point &Point::operator *=(const Point &rhs)
{
    m_x *= rhs.m_x;
    m_y *= rhs.m_y;

    return *this;
}

Point &Point::operator *=(double rhs)
{
    m_x *= rhs;
    m_y *= rhs;

    return *this;
}
double Point::x() const
{
    return m_x;
}
double Point::y() const
{
    return m_y;
}



std::ostream &operator <<(std::ostream &os, const Point &point)
{
    std::stringstream ss;
    ss << point;
    os << ss.str();
    return os;
}

std::stringstream &operator <<(std::stringstream &ss, const Point &point)
{
    ss << "[" << point.m_x << ", " << point.m_y << "]";
    return ss;
}


Point operator +(const Point &lhs, const Point &rhs)
{
    Point sum = lhs;
    sum += rhs;
    return sum;
}


Point operator -(const Point &lhs, const Point &rhs)
{
    Point sum = lhs;
    sum -= rhs;
    return sum;
}


Point operator *(const Point &lhs, double rhs)
{
    Point product = lhs;
    product *= rhs;
    return product;
}


Point operator *(double lhs, const Point &rhs)
{
    Point product = rhs;
    product *= lhs;
    return product;
}

Spline::Spline(const std::vector<Point> &controlPoints, unsigned int degree, double timeSpan) : m_controlPoints(controlPoints), m_degree(degree)
{
    if (controlPoints.size() < degree + 1) throw std::invalid_argument("Number of control points can not be less than spline degree + 1");
    std::vector<double> internalKnots = Util::linspace(0, timeSpan, controlPoints.size() - degree + 1);
    // use clamped spline, so make the first d knots and last d knots equal
    // this way we will make sure the spline cross the last control point
    for(unsigned int i = 0; i< degree; ++i)
    {
        internalKnots.push_back(internalKnots.back());
        internalKnots.insert(internalKnots.begin(), internalKnots[0]);
    }

    m_knots = internalKnots;
}

Point Spline::value(double t) const
{
    Point res;
    for(std::size_t i = 0; i < m_controlPoints.size(); ++i)
    {
        res += b_i_d(i, m_degree, t) * m_controlPoints[i];
    }

    return res;
}
std::vector<double> Spline::knots() const
{
    return m_knots;
}

double Spline::b_i_0(unsigned int i, double t) const
{
    if (i > m_controlPoints.size() + m_degree - 1)
    {
        throw std::invalid_argument("knots vector index out of range");
    }

    // if two knots are equal, return 0
    if (Util::isDoubleEqual(m_knots[i], m_knots[i+1])) return 0;

    if (m_knots[i] <= t && t <= m_knots[i+1]) return 1;
    else return 0;
}

double Spline::b_i_d(unsigned int i, unsigned int degree, double t) const
{
    if(degree == 0)
    {
        return b_i_0(i, t);
    }

    // if two time knots are equal
    if (Util::isDoubleEqual(m_knots[i + degree], m_knots[i]) && Util::isDoubleEqual(m_knots[i + degree + 1], m_knots[i + 1]))
    {
        return 0;
    }
    else if (Util::isDoubleEqual(m_knots[i + degree], m_knots[i]))
    {
        return (m_knots[i + 1 + degree] - t) / (m_knots[i + degree + 1] - m_knots[i + 1]) * b_i_d(i+1, degree - 1, t);
    }
    else if (Util::isDoubleEqual(m_knots[i + degree + 1], m_knots[i + 1]))
    {
        return (t - m_knots[i]) / (m_knots[i + degree] - m_knots[i]) * b_i_d(i, degree - 1, t);
    }
    else
    {
        return (t - m_knots[i]) / (m_knots[i + degree] - m_knots[i]) * b_i_d(i, degree - 1, t) + \
               (m_knots[i + 1 + degree] - t) / (m_knots[i + degree + 1] - m_knots[i + 1]) * b_i_d(i+1, degree - 1, t);
    }
}

