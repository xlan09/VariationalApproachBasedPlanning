#ifndef SPLINE_H
#define SPLINE_H

#include <vector>
#include <iostream>

/**
 * @brief The Point class
 */
class Point
{
public:
    Point();
    Point(double x);
    Point(double x, double y);

    double &operator[](unsigned int i);
    const double &operator [](unsigned int i) const;

    Point &operator +=(const Point &rhs);
    Point &operator +=(double rhs);
    Point &operator -=(const Point &rhs);
    Point &operator -=(double rhs);
    Point &operator *=(const Point &rhs);
    Point &operator *=(double rhs);

    friend std::ostream &operator <<(std::ostream &os, const Point &point);
    friend std::stringstream &operator <<(std::stringstream &ss, const Point &point);

    double x() const;

    double y() const;

private:
    double m_x;
    double m_y;
};

Point operator +(const Point &lhs, const Point &rhs);
Point operator -(const Point &lhs, const Point &rhs);
Point operator *(const Point &lhs, double rhs);
Point operator *(double lhs, const Point &rhs);

/**
 * @brief The Spline class
 */
class Spline
{
public:
    Spline(const std::vector<Point> &controlPoints, unsigned int degree, double timeSpan=1);
    Point value(double t) const;
    std::vector<double> knots() const;

private:
    std::vector<Point> m_controlPoints;
    unsigned int m_degree;
    std::vector<double> m_knots;

    /**
     * @brief b_i_0: basis function
     * B_i_0(t) = 1 if t_i <= t <= t_{i+1} else 0
     */
    double b_i_0(unsigned int i, double t) const;
    double b_i_d(unsigned int i, unsigned int degree, double t) const;
};

#endif // SPLINE_H
