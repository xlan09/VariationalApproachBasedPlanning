#include "DataStore.h"

DataStore::DataStore()
{

}

void DataStore::addPointToQueue(const std::pair<double, double> &point)
{
    std::lock_guard<std::mutex> lock(m_mu);
    m_queue.push(point);
}

std::pair<double, double> DataStore::popPoint()
{
    std::unique_lock<std::mutex> lock(m_mu);
    std::pair<double, double> res;
    if(!m_queue.empty())
    {
        res = m_queue.front();
        m_queue.pop();
    }
    else
    {
        throw std::logic_error("Can not pop since queue is empty!");
    }

    return res;
}

bool DataStore::isQueueEmpty() const
{
    std::lock_guard<std::mutex> lock(m_mu);
    return m_queue.empty();
}

