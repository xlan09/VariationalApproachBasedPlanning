#ifndef UTIL_H
#define UTIL_H
#include <vector>
#include "Eigen/Dense"
#include <utility>
#include <memory>
#include <map>

class VelocityProfile;
class AngularProfile;
class State;
class Point;
namespace spdlog {
class logger;
}

class Util
{
public:

    /**
     * @brief isDoubleEqual
     * Check whether two double numbers are equal
     */
    static bool isDoubleEqual(double lhs, double rhs);

    /**
     * @brief linspace
     * Generate evenly spaced points
     */
    static std::vector<double> linspace(double start, double end, unsigned int step);

    /**
     * @brief arange
     * Generate a vector of numbers in the range[start, end) with specified interval
     */
    static std::vector<double> arange(double start, double end, double interval);

    /**
     * @brief convertToVector
     * Convert to an Eigen vector
     */
    static Eigen::VectorXd convertToEigenVector(const std::vector<double> &vec);

    /**
     * @brief convertToVector
     * Convert to an Eigen vector
     */
    static Eigen::VectorXd convertToEigenVector(const State &state);

    /**
     * @brief convertToStdVector
     * Convert eigen vectors into std vector
     * @param eigenVec
     * @return
     */
    static std::vector<double> convertToStdVector(const Eigen::VectorXd &eigenVec);

    /**
     * @brief generateControlPoints
     */
    static std::vector<Point> generateControlPoints(const std::vector<double> &values);

    /**
     * @brief convertParametersToProfiles
     * Convert parameters to profiles
     */
    static std::pair<VelocityProfile, AngularProfile> convertParametersToProfiles(const State &startState, const State &goalState, const std::vector<double> &para);

    /**
     * @brief getLogger
     * @param loggerName
     * @param loggerFilename
     * @return
     */
    static std::shared_ptr<spdlog::logger> getLogger(const std::string &loggerName, const std::string &loggerFilename);

    /**
     * Find insertion position
     */
    template<typename T1, typename T2>
    static T1 findInsertionPosition(const std::map<T1, T2> &mapping, T1 val);

    /**
     * @brief toString
     * Convert a vector to string
     * @param val
     * @return
     */
    template<typename T>
    static std::string toString(const std::vector<T> &val);

    /**
     * @brief toString
     * convert matrix to string
     * @param mat
     * @return
     */
    template<typename MatT>
    static std::string toString(const MatT &mat);

    /**
     * Pseudoinverse of matrix
     *
     */
    template <typename MatT>
    static Eigen::Matrix<typename MatT::Scalar, MatT::ColsAtCompileTime, MatT::RowsAtCompileTime>
    pseudoinverse(const MatT &mat, typename MatT::Scalar tolerance = typename MatT::Scalar{1e-4});
};

#endif // UTIL_H
