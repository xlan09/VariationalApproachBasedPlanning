#include "Vehicle.h"
#include <cmath>

const double PI = 3.1415926535897;

Vehicle::Vehicle(const Pose &initPose, double initialLinearVelocity, double initialAngularVelocity) : m_pose(initPose), m_linearVelocity(initialLinearVelocity), m_angularVelocity(initialAngularVelocity)
{

}

Pose Vehicle::pose() const
{
    return m_pose;
}

double Vehicle::linearVelocity() const
{
    return m_linearVelocity;
}

void Vehicle::setLinearVelocity(double linearVelocity)
{
    m_linearVelocity = linearVelocity;
}

double Vehicle::angularVelocity() const
{
    return m_angularVelocity;
}

void Vehicle::setAngularVelocity(double angularVelocity)
{
    m_angularVelocity = angularVelocity;
}

Pose Vehicle::dynamics(const Pose &currPose, double linearVelocity, double angularVelocity, double dt) const
{
    double tolerance = std::pow(10, -12);
    double x = currPose.x(), y = currPose.y(), theta = currPose.theta();

    // update model
    double old_theta = theta;
    theta += angularVelocity * dt;

    if (std::abs(angularVelocity) < tolerance)
    {
        x += linearVelocity * std::cos(theta) * dt;
        y += linearVelocity * std::sin(theta) * dt;
    }
    else
    {
        double radius = linearVelocity / angularVelocity;
        x += radius * (std::sin(theta) - std::sin(old_theta));
        y -= radius * (std::cos(theta) - std::cos(old_theta));
    }

    if(theta > PI)
    {
        theta -= 2 * PI;
    }
    else if(theta < -1.0 * PI)
    {
        theta += 2 * PI;
    }

    return Pose(x, y, theta);
}




