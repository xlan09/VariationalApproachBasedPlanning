#ifndef ANGULARPROFILE_H
#define ANGULARPROFILE_H

#include <vector>
#include "Spline.h"

class Point;

class AngularProfile
{
public:
    AngularProfile(const std::vector<Point> &controlPoints, double t);

    /**
     * @brief angularVelocity
     * The angular velocity at a specific time for this angular profile
     */
    Point angularVelocity(double time) const;

private:
    Spline m_spline;
};

#endif // ANGULARPROFILE_H
