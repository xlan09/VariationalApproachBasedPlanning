#include "Solver.h"
#include "State.h"
#include "Util.h"
#include "Vehicle.h"
#include "VelocityProfile.h"
#include "AngularProfile.h"
#include "spdlog/spdlog.h"
#include "Eigen/Dense"
#include "spdlog/spdlog.h"
#include "DataStore.h"
#include <cmath>
#include <fstream>

const double PI = 3.1415926;
const std::shared_ptr<spdlog::logger> Solver::ms_logger = Util::getLogger("Solver", "LogSolver");

std::vector<double> Solver::solve(const Vehicle &vehicle, const State &startState, const State &goalState, double timeStep, const std::vector<double> &initPara, double paraVariance, double tolerance, int maxIter)
{
    if (timeStep <= 0 || paraVariance <= 0 || tolerance <= 0)
    {
        throw std::invalid_argument("time step, variance and tolerance must be positive");
    }

//    std::string loggerName = "Solver";
//    std::string loggerFilename = "LogSolver";
//    auto ms_logger = Util::getLogger(loggerName, loggerFilename);

    ms_logger->info("#############################################################");
    ms_logger->info("start state: {}, goal state: {}, time step: {}, para variance: {}, tolerance: {}", startState.toString(), goalState.toString(), timeStep, paraVariance, tolerance);

    std::vector<double> currPara = initPara;
    int count = 1;
    while(1)
    {
        ms_logger->info("*****************Iteration {}***********************", count);
        ms_logger->info("curr para: {}", Util::toString(currPara));

        std::pair<VelocityProfile, AngularProfile> profiles = Util::convertParametersToProfiles(startState, goalState, currPara);
        std::vector<std::pair<double, double>> trajectory;
        State simulatedFinalState = forwardSimulation(vehicle, startState, profiles.first, profiles.second, currPara.back(), timeStep, trajectory);
        State finalStateError = simulatedFinalState - goalState;

        ms_logger->info("final state error: {}", finalStateError.toString());

        if(finalStateError.isCloseToZero(tolerance))
        {
            ms_logger->info("stop because error is within tolerance {}, count={}", tolerance, count);
            break;
        }
        else if(count >= maxIter)
        {
            ms_logger->info("stop because maximum iterations {} reached", maxIter);
            break;
        }

        // calculate Jacobian matrix
        Eigen::MatrixXd jacobian(simulatedFinalState.size(), currPara.size());
        for(unsigned int i = 0; i < currPara.size(); ++i)
        {
            State derirativeToIthPara = calculatePartialDerirativeForJthPara(vehicle, startState, goalState, currPara, paraVariance, timeStep, i);
            jacobian.col(i) = Util::convertToEigenVector(derirativeToIthPara);
        }

        ms_logger->info("jacobian is: {}", Util::toString(jacobian));

        Eigen::VectorXd currParaVecForm = Util::convertToEigenVector(currPara);
//        currParaVecForm += (-1.0) * ((jacobian.adjoint() * jacobian).inverse() * (jacobian.adjoint()) ) * Util::convertToEigenVector(finalStateError);
        currParaVecForm += (-1.0) * Util::pseudoinverse<Eigen::MatrixXd>(jacobian, 1e-14) * Util::convertToEigenVector(finalStateError);

        currPara = Util::convertToStdVector(currParaVecForm);

        ms_logger->info("updated para is {}", Util::toString(currPara));
        count++;
    }

    ms_logger->flush();

    return currPara;
}

State Solver::forwardSimulation(const Vehicle &vehicle, const State &startState, const VelocityProfile &velocityProfile, const AngularProfile &angularProfile, double finalTime, double timeStep, std::vector<std::pair<double, double> > &trajectory)
{
    trajectory.clear();
    Pose currPose = startState.pose();
    std::vector<double> times = Util::arange(0, finalTime, timeStep);
    times.push_back(finalTime);

    trajectory.push_back(std::make_pair(currPose.x(), currPose.y()));
    for(int i = 0; i < (int)times.size() - 1; ++i)
    {
        double dt = times[i + 1] - times[i];
        currPose = vehicle.dynamics(currPose, velocityProfile.velocity(times[i]), angularProfile.angularVelocity(times[i])[0], dt);
        trajectory.push_back(std::make_pair(currPose.x(), currPose.y()));
    }

    return State(currPose, velocityProfile.velocity(finalTime), angularProfile.angularVelocity(finalTime)[0]);
}

State Solver::calculatePartialDerirativeForJthPara(
    const Vehicle &vehicle,
    const State &startState,
    const State &goalState,
    const std::vector<double> &currPara,
    double paraVariance,
    double timeStep,
    unsigned int j)
{
    std::vector<double> rightVariedPara = currPara;
    std::vector<double> leftVariedPara = currPara;
    rightVariedPara[j] += paraVariance;
    leftVariedPara[j] -= paraVariance;

    std::pair<VelocityProfile, AngularProfile> profiles = Util::convertParametersToProfiles(startState, goalState, rightVariedPara);
    std::vector<std::pair<double, double>> trajectory;
    State rightError = forwardSimulation(vehicle, startState, profiles.first, profiles.second, rightVariedPara.back(), timeStep, trajectory) - goalState;

    profiles = Util::convertParametersToProfiles(startState, goalState, leftVariedPara);
    State leftError = forwardSimulation(vehicle, startState, profiles.first, profiles.second, leftVariedPara.back(), timeStep, trajectory) - goalState;

    return (rightError - leftError) / (2.0 * paraVariance);
}

SolverService::SolverService(double startPosX, double startPosY, double goalPosX, double goalPosY, double maxLinearVel) : m_startPosX(startPosX), m_startPosY(startPosY), m_goalPosX(goalPosX), m_goalPosY(goalPosY), m_maxLinearVel(maxLinearVel)
{
    m_headingResolution = PI / 20;
    m_w0 = 0;
    m_wf = 0;
    m_timeStep = 0.1;

    if(m_headingResolution <= 0)
    {
        throw std::invalid_argument("Heading resolution must be positive");
    }

    m_dataStore = std::shared_ptr<DataStore>(new DataStore());

    // initialize logger
    std::string loggerName = "SolverService", loggerFilename = "LogService";
    m_logger = Util::getLogger(loggerName, loggerFilename);

    initLookupTable();
    m_logger->flush();
}

void SolverService::run(double startHeading, double goalHeading)
{
    // find init parameters in the looup table
    std::pair<double, double> refHeadings = findClosestParametersInLookupTable(startHeading, goalHeading);
    m_logger->info("startHeading={} : refStartHeading={}, goalHeading={} : refGoalHeading={}", startHeading, refHeadings.first, goalHeading, refHeadings.second);

    // fix goalHeading, and vary starHeading from the refStart in the lookup table to the expected startHeading
    std::vector<double> para = incrementallySearchParameters(refHeadings.second, refHeadings.first, startHeading, m_headingResolution, m_paraLookupTable.at(refHeadings.first).at(refHeadings.second), true);
    m_logger->info("Incrementally search parameters from refStartHeading={} to startHeading={} with fixed goalHeading={}, para: {}", refHeadings.first, startHeading, refHeadings.second, Util::toString(para));

    // fix startHeading, vary goalHeading from the refStart in the lookup table to the expected goalHeading
    para = incrementallySearchParameters(startHeading, refHeadings.second, goalHeading, m_headingResolution, para, false);
    m_logger->info("Incrementally search parameters from refGoalHeading={} to goalHeading={} with fixed startHeading={}, para: {}", refHeadings.second, goalHeading, startHeading, Util::toString(para));

    addParaToLookUpTable(startHeading, goalHeading, para);

    std::vector<std::pair<double, double>> trajectory;
    getTrajectory(startHeading, goalHeading, para, trajectory);

//    printTrajectoryToFile(trajectory);
    addTrajectoryToQueue(trajectory);
    m_logger->flush();
}

bool SolverService::havePointsToDraw() const
{
   return !(m_dataStore->isQueueEmpty());
}

std::pair<double, double> SolverService::popPoint() const
{
    return m_dataStore->popPoint();
}

std::vector<double> SolverService::runSolver(double startHeading, double goalHeading, const std::vector<double> initPara) const
{
    double paraVariance = 0.01, tolerance = 0.015;

    Pose startPose(m_startPosX, m_startPosY, startHeading);
    Vehicle vehicle(startPose, m_maxLinearVel, m_w0);
    State startState(startPose, m_maxLinearVel, m_w0);

    State goalState(Pose(m_goalPosX, m_goalPosY, goalHeading), m_maxLinearVel, m_wf);
    std::vector<double> optimalPara = Solver::solve(vehicle, startState, goalState, m_timeStep, initPara, paraVariance, tolerance);

    return optimalPara;
}

void SolverService::initLookupTable()
{
    m_logger->info("Initializing lookup table for startPosX={}, startPosY={}, goalPosX={}, goalPosY={}, headingResolution={}, maxLinearVelocityOfVehicle={}", m_startPosX, m_startPosY, m_goalPosX, m_goalPosY, m_headingResolution, m_maxLinearVel);

    double headingOfStartToGoal = std::atan2(m_goalPosY - m_startPosY, m_goalPosX - m_startPosX);

    double startHeading = headingOfStartToGoal;
    double dist = std::sqrt(std::pow(m_goalPosY - m_startPosY, 2) + std::pow(m_goalPosX - m_startPosX, 2));

    double initParaArray[] = {0, 0.01, 0.02, 0.03, dist / m_maxLinearVel};
    std::vector<double> initPara(initParaArray, initParaArray + 5);

    std::vector<double> goalStateHeading = Util::arange(0, PI, m_headingResolution);
    for(unsigned int i = 0; i < goalStateHeading.size(); ++i)
    {
        double goalHeading = goalStateHeading[i] + headingOfStartToGoal;
        if (goalHeading > PI) goalHeading -= 2 * PI;

        std::vector<double> optimalPara = runSolver(startHeading, goalHeading, initPara);
        initPara = optimalPara;

        addParaToLookUpTable(startHeading, goalHeading, optimalPara);
    }

    // now we have look up tables for startHeading=0, goalHeading=0-180
    // we can get look up tables for startHeading=0, goalHeading=-180-0 based on symmetry about x axis if the start pos and goal pos are on x axis
    // If optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, PI/20, 0.6, 0] where state=[x, y, theta, linearVel, angularVel] is [x1, x2, x3, x4, finalTime], then the optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, -PI/20, 0.6, 0] should be [-x1, -x2, -x3, -x4, finalTime]. The first four parameters are the middle control points for the 5-th degree spline used to parametrize the angular velocity.
    if(Util::isDoubleEqual(headingOfStartToGoal, 0) || Util::isDoubleEqual(headingOfStartToGoal, PI) || Util::isDoubleEqual(headingOfStartToGoal, -PI))
    {
        std::map<double, std::vector<double>> parameters = m_paraLookupTable[startHeading];
        for(auto iter = parameters.begin(); iter != parameters.end(); ++iter)
        {
            std::vector<double> optimalPara = iter->second;
            for(int i = 0; i < (int)optimalPara.size()-1; ++i)
            {
                optimalPara[i] *= (-1.0);
            }

            double goalHeadingSymmetricAboutXaxis = -1.0 * (iter->first);
            addParaToLookUpTable(startHeading, goalHeadingSymmetricAboutXaxis, optimalPara);
        }
    }

    // If optimal para for startState=[0, 0, 0, 0.6, 0] and goalState=[2, 0, PI/20, 0.6, 0] where state=[x, y, theta, linearVel, angularVel] is [x1, x2, x3, x4, finalTime], then the optimal para for startState=[0, 0, PI/20, 0.6, 0] and goalState=[2, 0, 0, 0.6, 0] should be [-x4, -x3, -x2, -x1, finalTime]. The first four parameters are the middle control points for the 5-th degree spline used to parametrize the angular velocity.
    std::map<double, std::vector<double>> parameters = m_paraLookupTable[startHeading];
    for(auto iter = parameters.begin(); iter != parameters.end(); ++iter)
    {
        std::vector<double> optimalPara = iter->second;
        for(int i = 0, j = optimalPara.size() - 2; i < j; ++i, --j)
        {
            double temp = optimalPara[i];
            optimalPara[i] = -1.0 * optimalPara[j];
            optimalPara[j] = -1.0 * temp;
        }
        addParaToLookUpTable(iter->first, startHeading, optimalPara);
    }

    m_logger->info("Initializing lookup table done");
    m_logger->info("---------------------------------------------------------");
}

std::pair<double, double> SolverService::findClosestParametersInLookupTable(double startHeading, double goalHeading) const
{
    double refStartHeading = Util::findInsertionPosition(m_paraLookupTable, startHeading);
    double refGoalHeading = Util::findInsertionPosition(m_paraLookupTable.at(refStartHeading), goalHeading);

    return std::pair<double, double>(refStartHeading, refGoalHeading);
}

std::vector<double> SolverService::incrementallySearchParameters(double fixedHeading, double variedInitialHeading, double variedFinalHeading, double headingResolution, const std::vector<double> &paraToStart, bool incrementStartHeading) const
{
    if (headingResolution <= 0) throw std::invalid_argument("heading resolution must be positive!");

    double headingInterval = 0;
    if(variedInitialHeading < variedFinalHeading)
    {
        headingInterval = headingResolution;
    }
    else if(variedInitialHeading > variedFinalHeading)
    {
        headingInterval = (-1.0) * headingResolution;
    }

    std::vector<double> initPara = paraToStart;
    if (Util::isDoubleEqual(variedInitialHeading, variedFinalHeading) == false)
    {
        std::vector<double> discretizedHeadingVec = Util::arange(variedInitialHeading, variedFinalHeading, headingInterval);
        discretizedHeadingVec.push_back(variedFinalHeading);

        std::vector<double> optimalPara;
        for(unsigned int i = 0; i < discretizedHeadingVec.size(); ++i)
        {
            if(incrementStartHeading)
            {
                optimalPara = runSolver(discretizedHeadingVec[i], fixedHeading, initPara);
            }
            else
            {
                optimalPara = runSolver(fixedHeading, discretizedHeadingVec[i], initPara);
            }
            initPara = optimalPara;
        }
    }

    return initPara;
}

void SolverService::getTrajectory(double startHeading, double goalHeading, const std::vector<double> &para, std::vector<std::pair<double, double> > &trajectory) const
{
    trajectory.clear();
    Pose startPose(m_startPosX, m_startPosY, startHeading);
    State startState(startPose, m_maxLinearVel, m_w0);
    State goalState(Pose(m_goalPosX, m_goalPosY, goalHeading), m_maxLinearVel, m_wf);
    Vehicle vehicle(startPose, m_maxLinearVel, m_w0);
    std::pair<VelocityProfile, AngularProfile> profiles = Util::convertParametersToProfiles(startState, goalState, para);
    State finalState = Solver::forwardSimulation(vehicle, startState, profiles.first, profiles.second, para.back(), m_timeStep, trajectory);
}

void SolverService::printTrajectoryToFile(const std::vector<std::pair<double, double> > &trajectory) const
{
    // open file to store trajectory
    std::ofstream file("Trajectory.txt");
    for(std::size_t j = 0; j < trajectory.size(); ++j)
    {
        m_dataStore->addPointToQueue(trajectory[j]);
        if(file.is_open())
        {
            // print trajectory length to prepare for plotting
            file << trajectory[j].first << ", " << trajectory[j].second << ", " << trajectory.size() << std::endl;
        }
        else
        {
            throw std::logic_error("Can not open file to store trajectory by planner");
        }
    }
    file.close();
}

void SolverService::addTrajectoryToQueue(const std::vector<std::pair<double, double> > &trajectory) const
{
    for(std::size_t j = 0; j < trajectory.size(); ++j)
    {
        m_dataStore->addPointToQueue(trajectory[j]);
    }
    m_logger->info("Trajectory with {} points added to queue", trajectory.size());
}

void SolverService::addParaToLookUpTable(double startHeading, double goalHeading, const std::vector<double> &para)
{
    if (m_paraLookupTable.find(startHeading) == m_paraLookupTable.end())
    {
        m_paraLookupTable[startHeading] = std::map<double, std::vector<double>>();
    }

    m_paraLookupTable[startHeading][goalHeading] = para;

    m_logger->info("Lookup table added: startHeading, goalHeading: [{}, {}] -> {}", startHeading, goalHeading, Util::toString(para));
}
