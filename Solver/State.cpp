#include "State.h"
#include "Util.h"
#include <stdexcept>
#include <cmath>

State::State(const Pose &pose, double linearVelocity, double angularVelocity) : m_pose(pose), m_linearVelocity(linearVelocity), m_angularVelocity(angularVelocity)
{

}

State &State::operator +=(const State &state)
{
    m_pose += state.m_pose;
    m_linearVelocity += state.m_linearVelocity;
    m_angularVelocity += state.m_angularVelocity;

    return *this;
}

State &State::operator -=(const State &state)
{
    m_pose -= state.m_pose;
    m_linearVelocity -= state.m_linearVelocity;
    m_angularVelocity -= state.m_angularVelocity;

    return *this;
}

State &State::operator *=(double num)
{
    m_pose *= num;
    m_linearVelocity *= num;
    m_angularVelocity *= num;

    return *this;
}

bool State::isCloseToZero(double tolerance) const
{
    bool res = true;
    Eigen::VectorXd vec = Util::convertToEigenVector(*this);
    for(int i = 0; i < vec.rows(); ++i)
    {
        if(std::abs(vec(i)) > tolerance)
        {
            res = false;
            break;
        }
    }

    return res;
}

unsigned int State::size() const
{
    Eigen::VectorXd vec = Util::convertToEigenVector(*this);
    return vec.rows();
}

Pose State::pose() const
{
    return m_pose;
}
double State::linearVelocity() const
{
    return m_linearVelocity;
}
double State::angularVelocity() const
{
    return m_angularVelocity;
}

std::string State::toString() const
{
    return m_pose.toString() + ", linearVec: " + std::to_string(m_linearVelocity) + ", angularVec: " + std::to_string(m_angularVelocity);
}

State operator +(const State &lhs, const State &rhs)
{
    State sum = lhs;
    sum += rhs;

    return sum;
}


State operator -(const State &lhs, const State &rhs)
{
    State sum = lhs;
    sum -= rhs;

    return sum;
}


State operator *(const State &val, double num)
{
    State product = val;
    product *= num;

    return product;
}


State operator *(double num, const State &val)
{
    return val * num;
}


State operator /(const State &val, double num)
{
    if (Util::isDoubleEqual(num, 0))
    {
        throw std::invalid_argument("State can not be divided by 0");
    }

    return val * (1.0 / num);
}


State operator /(double num, const State &val)
{
    return val / num;
}
