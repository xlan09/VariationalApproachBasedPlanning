#ifndef STATE_H
#define STATE_H

#include "Pose.h"

class State
{
public:
    State(const Pose &pose, double linearVelocity, double angularVelocity);

    State &operator +=(const State &state);
    State &operator -=(const State &state);
    State &operator *=(double num);

    /**
     * @brief isCloseToZero
     * Check whether state is close to zero
     * @return
     */
    bool isCloseToZero(double tolerance) const;

    unsigned int size() const;

    Pose pose() const;

    double linearVelocity() const;

    double angularVelocity() const;

    std::string toString() const;

private:
    Pose m_pose;
    double m_linearVelocity;
    double m_angularVelocity;
};

State operator +(const State &lhs, const State &rhs);
State operator -(const State &lhs, const State &rhs);
State operator *(const State &val, double num);
State operator *(double num, const State &val);
State operator /(const State &val, double num);
State operator /(double num, const State &val);

#endif // STATE_H
