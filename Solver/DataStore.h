#ifndef DATASTORE_H
#define DATASTORE_H
#include <mutex>
#include <condition_variable>
#include <queue>

class DataStore
{
public:
    DataStore();
    DataStore(const DataStore &copy) = delete;
    DataStore &operator =(const DataStore &val)=delete;

    /**
     * @brief addPointToQueue
     * @param point
     */
    void addPointToQueue(const std::pair<double, double> &point);

    /**
     * @brief popPoint
     * Pop point out of queue
     * @return
     */
    std::pair<double, double> popPoint();

    /**
     * @brief isQueueEmpty
     * @return
     */
    bool isQueueEmpty() const;

private:
    mutable std::mutex m_mu;
    std::queue<std::pair<double, double>> m_queue;
};

#endif // DATASTORE_H
