#-------------------------------------------------
#
# Project created by QtCreator 2016-11-23T16:52:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = UI
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    UIModel.cpp \
    UIView.cpp

HEADERS  += MainWindow.h \
    UIModel.h \
    UIView.h

CONFIG += C++11

LIBS += $$PWD/../build*/Solver/libSolver.a -lpthread
INCLUDEPATH += $$PWD/../Solver $$PWD/../lib
