#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
class QSpinBox;
class QSlider;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QSpinBox *createSpinBox() const;
    QSlider *createSlider() const;
};

#endif // MAINWINDOW_H
