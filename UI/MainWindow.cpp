#include "MainWindow.h"
#include <QPushButton>
#include <UIModel.h>
#include <UIView.h>
#include <QGridLayout>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("GUI");

    QPushButton *run = new QPushButton(this);
    run->setText("Run");
    QPushButton *reset = new QPushButton(this);
    reset->setText("Reset");

    // create slider and spinbox for start heading
    QLabel *startHeadingLabel = new QLabel("Start State Heading");
    QSlider *startHeadingSlider = createSlider();
    QSpinBox *startHeadingSpinBox = createSpinBox();

    connect(startHeadingSlider, SIGNAL(valueChanged(int)), startHeadingSpinBox, SLOT(setValue(int)));
    connect(startHeadingSpinBox, SIGNAL(valueChanged(int)), startHeadingSlider, SLOT(setValue(int)));

    // create slider and spinbox for goal heading
    QLabel *goalHeadingLabel = new QLabel("Goal State Heading");
    QSlider *goalHeadingSlider = createSlider();
    QSpinBox *goalHeadingSpinBox = createSpinBox();

    connect(goalHeadingSlider, SIGNAL(valueChanged(int)), goalHeadingSpinBox, SLOT(setValue(int)));
    connect(goalHeadingSpinBox, SIGNAL(valueChanged(int)), goalHeadingSlider, SLOT(setValue(int)));

    UIModel *model = new UIModel(this, startHeadingSpinBox, goalHeadingSpinBox);
    model->setSceneRect(0, 0, 600, 600);

    UIView *viewer = new UIView(model, this);

    QGridLayout *layout = new QGridLayout();
    layout->addWidget(run, 0, 0);
    layout->addWidget(reset, 0, 1);
    layout->addWidget(startHeadingLabel, 1, 0);
    layout->addWidget(startHeadingSpinBox, 1, 1);
    layout->addWidget(startHeadingSlider, 1, 2);
    layout->addWidget(goalHeadingLabel, 1, 3);
    layout->addWidget(goalHeadingSpinBox, 1, 4);
    layout->addWidget(goalHeadingSlider, 1, 5);
    layout->addWidget(viewer, 2, 0, 50, 50);

    QWidget *centralWidget = new QWidget(this);
    centralWidget->setLayout(layout);
    this->setCentralWidget(centralWidget);

    //  make the buttons do things
    connect(run, SIGNAL(clicked()), model, SLOT(slot_run()));
    connect(reset, SIGNAL(clicked()), model, SLOT(slot_reset()));
    connect(startHeadingSpinBox, SIGNAL(valueChanged(int)), model, SLOT(slot_setStartHeadingDirection(int)));
    connect(goalHeadingSpinBox, SIGNAL(valueChanged(int)), model, SLOT(slot_setGoalHeadingDirection(int)));
}

MainWindow::~MainWindow()
{

}

QSpinBox *MainWindow::createSpinBox() const
{
    QSpinBox *spinBox = new QSpinBox;
    spinBox->setRange(-180, 180);
    spinBox->setSingleStep(1);
    spinBox->setValue(0);

    return spinBox;
}

QSlider *MainWindow::createSlider() const
{
    QSlider *slider = new QSlider(Qt::Horizontal);
    slider->setFocusPolicy(Qt::StrongFocus);
    slider->setTickPosition(QSlider::TicksBothSides);
    slider->setTickInterval(10);
    slider->setSingleStep(1);
    slider->setMinimum(-180);
    slider->setMaximum(180);
    slider->setMinimumWidth(50);

    return slider;
}
