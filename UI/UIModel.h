#ifndef UIMODEL_H
#define UIMODEL_H

#include <QGraphicsScene>
#include <memory>

class SolverService;
class QSpinBox;
class QGraphicsEllipseItem;
class QGraphicsLineItem;

const double PI = 3.1415926;

class UIModel : public QGraphicsScene
{
    Q_OBJECT
public slots:
    void slot_run();
    void slot_reset();
    void slot_setStartHeadingDirection(int val);
    void slot_setGoalHeadingDirection(int val);

public:
    UIModel(QObject *parent, QSpinBox *startHeadingSpinBox, QSpinBox *goalHeadingSpinBox);

 private:
    float m_startPosX;
    float m_startPosY;
    float m_startTheta;
    float m_goalPosX;
    float m_goalPosY;
    float m_goalTheta;
    float m_tangentLineLength;
    QGraphicsLineItem *m_startHeadingDirection;
    QGraphicsLineItem *m_goalHeadingDirection;
    std::shared_ptr<SolverService> m_solver;

    QGraphicsLineItem * generateHeadingLine();
    void drawHeading(QGraphicsLineItem *headingDirection, float startPointX, float startPointY, float lineLength, float headingInRadian);

    /**
     * @brief drawStates
     * draw the start position and goal position, start heading and goal heading
     */
    void drawStates();

    /**
     * @brief drawTrajectory
     */
    void drawTrajectory();

    template<typename T>
    float convertDegreeToRadian(T val)
    {
        return val / 180.0 * PI;
    }
};

#endif // UIMODEL_H
