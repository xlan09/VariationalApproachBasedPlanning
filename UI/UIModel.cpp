#include "UIModel.h"
#include <QSpinBox>
#include <cmath>
#include <QGraphicsLineItem>
#include "Solver.h"
#include <future>

void UIModel::slot_run()
{
    std::future<void> future(std::async(std::launch::async, &SolverService::run, *m_solver, m_startTheta, m_goalTheta));

    future.get(); // wait for running finish
    drawTrajectory();
}

void UIModel::slot_reset()
{
    clear();
    drawStates();
}

void UIModel::slot_setStartHeadingDirection(int val)
{
    m_startTheta = convertDegreeToRadian(val);
    drawHeading(m_startHeadingDirection, m_startPosX, m_startPosY, m_tangentLineLength, m_startTheta);
}

void UIModel::slot_setGoalHeadingDirection(int val)
{
    m_goalTheta = convertDegreeToRadian(val);
    drawHeading(m_goalHeadingDirection, m_goalPosX, m_goalPosY, m_tangentLineLength, m_goalTheta);
}

UIModel::UIModel(QObject *parent, QSpinBox *startHeadingSpinBox, QSpinBox *goalHeadingSpinBox) : QGraphicsScene(parent)
{
    m_startPosX = 80;
    m_startPosY = 300;
    m_startTheta = convertDegreeToRadian(startHeadingSpinBox->value());
    m_goalPosX = 480;
    m_goalPosY = 300;
    m_goalTheta = convertDegreeToRadian(goalHeadingSpinBox->value());
    m_tangentLineLength = 50;

    drawStates();

    m_solver = std::shared_ptr<SolverService>(new SolverService(m_startPosX, m_startPosY, m_goalPosX, m_goalPosY, 20));
}

QGraphicsLineItem *UIModel::generateHeadingLine()
{
    QGraphicsLineItem * headingDirection = addLine(0, 0, 0, 0);
    QPen newPen(Qt::PenStyle::SolidLine);
    newPen.setColor(Qt::red);
    newPen.setWidth(2);
    headingDirection->setPen(newPen);
    addItem(headingDirection);

    return headingDirection;
}

void UIModel::drawHeading(QGraphicsLineItem * headingDirection, float startPointX, float startPointY, float lineLength, float headingInRadian)
{
    float endPointX = startPointX + std::cos(headingInRadian) * lineLength;
    float endPointY = startPointY + std::sin(headingInRadian) * lineLength;
    headingDirection->setLine(startPointX, startPointY, endPointX, endPointY);
}

void UIModel::drawStates()
{
    addEllipse(m_startPosX, m_startPosY, 5, 5);
    addEllipse(m_goalPosX, m_goalPosY, 5, 5);

    m_startHeadingDirection = generateHeadingLine();
    drawHeading(m_startHeadingDirection, m_startPosX, m_startPosY, m_tangentLineLength, m_startTheta);
    m_goalHeadingDirection = generateHeadingLine();
    drawHeading(m_goalHeadingDirection, m_goalPosX, m_goalPosY, m_tangentLineLength, m_goalTheta);
}

void UIModel::drawTrajectory()
{
    while(m_solver->havePointsToDraw())
    {
        std::pair<double, double> point = m_solver->popPoint();
        QGraphicsEllipseItem *item  = addEllipse(point.first, point.second, 2.5, 2.5);
        item->setBrush(Qt::blue);
        addItem(item);
    }
}
