TEMPLATE = subdirs

SUBDIRS += \
    UI \
    Solver \
    Test

UI.SUBDIRS = UI
Solver.SUBDIRS = Solver
Test.SUBDIRS = Test

Test.depends = Solver
UI.depends = Solver
